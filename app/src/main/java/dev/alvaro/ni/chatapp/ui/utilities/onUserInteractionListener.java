package dev.alvaro.ni.chatapp.ui.utilities;

import dev.alvaro.ni.chatapp.entities.User;

public interface onUserInteractionListener {
    void onUserTouched(User user);
}
