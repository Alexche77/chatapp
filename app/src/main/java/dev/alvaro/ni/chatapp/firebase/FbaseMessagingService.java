package dev.alvaro.ni.chatapp.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import androidx.core.app.NotificationCompat;
import dev.alvaro.ni.chatapp.R;
import dev.alvaro.ni.chatapp.ui.activities.ChatActivity;
import dev.alvaro.ni.chatapp.utilities.Constants;
import dev.alvaro.ni.chatapp.utilities.DatabaseReferences;

public class FbaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = FbaseMessagingService.class.getSimpleName();

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.i(TAG, "onNewToken: New token recieved "+s);
        String uid = FirebaseAuth.getInstance()
                .getUid();
        if (uid != null) {
            CollectionReference mUsersRef = FirebaseFirestore.getInstance()
                    .collection(DatabaseReferences.USERS);
            mUsersRef.document(uid)
                    .update("token", s)
                    .addOnCompleteListener(
                            task -> Log.i(TAG, "onNewToken: New token " + s));

        }
    }


    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String notificationBody = "";
        String notificationTitle = "";
        String notificationData = "";
        notificationData = remoteMessage.getData()
                .toString();
        RemoteMessage.Notification n = remoteMessage.getNotification();
        if (n != null) {
            notificationTitle = remoteMessage.getNotification()
                    .getTitle();
            notificationBody = remoteMessage.getNotification()
                    .getBody();
        }
        Log.d(TAG, "onMessageReceived: data: " + notificationData);
        Log.d(TAG, "onMessageReceived: notification body: " + notificationBody);
        Log.d(TAG, "onMessageReceived: notification title: " + notificationTitle);


        String dataType = remoteMessage.getData()
                .get(Constants.NOTIF_TYPE);
        if (Constants.MESSAGE_TYPE.DIRECT_MESSAGE.toString()
                .equals(dataType)) {
            Log.d(TAG, "onMessageReceived: new incoming message.");
            String title = remoteMessage.getData()
                    .get(Constants.CHAT_TITLE);
            String message = remoteMessage.getData()
                    .get(Constants.CHAT_MESSAGE);
            String messageId = remoteMessage.getData()
                    .get(Constants.CHAT_UID);
            sendMessageNotification(title, message, messageId);
        }
    }

    private void sendMessageNotification(String title, String message, String messageId) {
        Log.d(TAG, "sendChatmessageNotification: building a chatmessage notification");

        //get the notification id
        int notificationId = buildNotificationId(messageId);

        // Instantiate a Builder object.
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,
                Constants.DEF_NOTIF_ID);
        // Creates an Intent for the Activity
        Intent pendingIntent = new Intent(this, ChatActivity.class);
        // Sets the Activity to start in a new, empty task
        pendingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        // Creates the PendingIntent
        PendingIntent notifyPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        pendingIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        //add properties to the builder
        builder.setSmallIcon(R.drawable.ic_new_message_white)
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.ic_new_message_white))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentTitle(title)
                .setColor(getColor(R.color.colorAccent))
                .setAutoCancel(true)
                .setSubText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setOnlyAlertOnce(true);

        builder.setContentIntent(notifyPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(notificationId, builder.build());

    }


    private int buildNotificationId(String id) {
        Log.d(TAG, "buildNotificationId: building a notification id.");

        int notificationId = 0;
        for (int i = 0; i < 9; i++) {
            notificationId = notificationId + id.charAt(0);
        }
        Log.d(TAG, "buildNotificationId: id: " + id);
        Log.d(TAG, "buildNotificationId: notification id:" + notificationId);
        return notificationId;
    }
}
