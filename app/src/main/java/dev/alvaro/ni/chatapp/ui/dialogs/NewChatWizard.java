package dev.alvaro.ni.chatapp.ui.dialogs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dev.alvaro.ni.chatapp.R;
import dev.alvaro.ni.chatapp.entities.Chat;
import dev.alvaro.ni.chatapp.entities.User;
import dev.alvaro.ni.chatapp.ui.activities.ChatActivity;
import dev.alvaro.ni.chatapp.ui.adapters.UsersAdapter;
import dev.alvaro.ni.chatapp.ui.utilities.onUserInteractionListener;
import dev.alvaro.ni.chatapp.utilities.DatabaseReferences;

public class NewChatWizard extends BottomSheetDialogFragment implements onUserInteractionListener {

    private static final String TAG = NewChatWizard.class.getSimpleName();
    private static final String ARG_CHAT_UID = "ARG_CHAT_UID";
    private static final String ARG_CHAT_RECIPIENT_UID = "ARG_CHAT_RECIPIENT_UID";
    private UsersAdapter adapter;
    private Context mContext;

    public NewChatWizard() {
    }

    public static NewChatWizard newInstance() {
        return new NewChatWizard();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.new_chat_wizard, container);
        RecyclerView userList = view.findViewById(R.id.userList);
        userList.setLayoutManager(new LinearLayoutManager(mContext));
        CollectionReference mUsersQuery = FirebaseFirestore.getInstance()
                .collection(DatabaseReferences.USERS);
        FirestoreRecyclerOptions<User> options =
                new FirestoreRecyclerOptions.Builder<User>()
                        .setQuery(mUsersQuery, User.class)
                        .build();

        FirebaseUser mainUser = FirebaseAuth.getInstance()
                .getCurrentUser();
        if (mainUser != null) {
            adapter = new UsersAdapter(options, this, mainUser.getUid(), mContext);
            userList.setAdapter(adapter);
        }
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (adapter != null) {

            adapter.startListening();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (adapter != null) {

            adapter.stopListening();
        }
    }

    @Override
    public void onUserTouched(User user) {
        Log.i(TAG, "onUserTouched: Iniciando chat con " + user.getName());
        CollectionReference chatsReference = FirebaseFirestore.getInstance()
                .collection(DatabaseReferences.CHATS);

        FirebaseUser mainUserFromFirestore = FirebaseAuth.getInstance()
                .getCurrentUser();

        if (mainUserFromFirestore == null) {
            Log.e(TAG, "onUserTouched: Error retrieving user");
            return;
        }
        chatsReference
                .whereEqualTo(Chat.CHAT_TYPE, Chat.TYPE.DM.ordinal())
                .whereArrayContains(Chat.MEMBERS, user.getUid())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Chat c = new Chat();
                        QuerySnapshot querySnap = task.getResult();
                        boolean newChat = querySnap != null && querySnap.isEmpty();
                        if (querySnap != null) {
                            for (DocumentSnapshot ds :
                                    querySnap.getDocuments()) {
                                Chat chat = ds.toObject(Chat.class);
                                if (chat != null) {
                                    Log.i(TAG, "accept: Analizando chat"+chat.getUid());
                                    for (String uid :
                                            chat.getMembers()) {
                                        if (uid.equals(mainUserFromFirestore.getUid())){
                                            Log.i(TAG, "onUserTouched: Chat encontrado");
                                            newChat = false;
                                            c = chat;
                                            break;
                                        }
                                    }
                                }

                            }
                        }


                        if (newChat) {

                            Log.i(TAG,
                                    "onUserTouched: Es un nuevo chat porque este usuario no tiene chats");
                            c.setMessageText(getString(R.string.new_chat));
                            c.setRecipientUid(user.getUid());
                            c.setTitle(user.getName());
                            c.setRecipientName(user.getName());
                            c.setMembers(Arrays.asList(user.getUid(), mainUserFromFirestore.getUid()));
                            c.setTimestamp(System.currentTimeMillis());
                            String chatid = chatsReference.document()
                                    .getId();
                            c.setUid(chatid);

                            FirebaseFirestore.getInstance()
                                    .collection(DatabaseReferences.CHATS).document(c.getUid()).set(c);


                            FirebaseFirestore.getInstance()
                                    .collection(DatabaseReferences.USERS)
                                    .document(mainUserFromFirestore.getUid())
                                    .collection(DatabaseReferences.LATEST_CHATS)
                                    .document(c.getUid())
                                    .set(c);

                            c.setTitle(mainUserFromFirestore.getDisplayName());

                            FirebaseFirestore.getInstance()
                                    .collection(DatabaseReferences.USERS)
                                    .document(user.getUid())
                                    .collection(DatabaseReferences.LATEST_CHATS)
                                    .document(c.getUid())
                                    .set(c);


                            Log.i(TAG,
                                    "onUserTouched: Iniciando activity de chat con un nuevo chat creado " + chatid);
                        }else
                            Log.i(TAG, "onUserTouched: Ya hay un chat entre estos dos usuarios");



                        Intent i = new Intent(mContext,
                                ChatActivity.class);
                        Bundle b = new Bundle();
                        b.putString(ARG_CHAT_UID, c.getUid());
                        b.putString(ARG_CHAT_RECIPIENT_UID,
                                c.getRecipientUid());
                        i.putExtras(b);
                        startActivity(i);


                    }else{
                        Log.e(TAG, "onUserTouched: Error en la ejecucion de query");
                    }


                });


    }
}
