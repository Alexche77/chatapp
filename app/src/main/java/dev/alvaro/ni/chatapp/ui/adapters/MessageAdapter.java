package dev.alvaro.ni.chatapp.ui.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import dev.alvaro.ni.chatapp.R;
import dev.alvaro.ni.chatapp.entities.Message;
import dev.alvaro.ni.chatapp.utilities.Utils;

public class MessageAdapter extends FirestoreRecyclerAdapter<Message, MessageAdapter.GenericViewHolder> {
    private final String mUserUid;
    private String TAG = MessageAdapter.class.getName();

    public MessageAdapter(@NonNull FirestoreRecyclerOptions<Message> options, String userId) {
        super(options);
        this.mUserUid = userId;
    }


    @Override
    protected void onBindViewHolder(@NonNull GenericViewHolder holder, int i, @NonNull Message message) {
        if (message.getSenderUid().equals(mUserUid)) {
            Log.i(TAG, "onBindViewHolder: Message was sent by you");
            (holder).bindSent(message);
        } else {
            Log.i(TAG, "onBindViewHolder: Message was sent to you");
            (holder).bindReceived(message);
        }
    }
    

    @NonNull
    @Override
    public GenericViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        

        return new GenericViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_item, parent, false));
    }


    class GenericViewHolder extends RecyclerView.ViewHolder {
        final TextView messageText, timeText;
        final TextView messageTextReceived, timeTextReceived;
        final View receivedLayout;
        final View sentLayout;

        GenericViewHolder(@NonNull View itemView) {
            super(itemView);

            //Received
            messageTextReceived = (TextView) itemView.findViewById(R.id.text_message_body);
            timeTextReceived = (TextView) itemView.findViewById(R.id.text_message_time);

            //Sent
            messageText = (TextView) itemView.findViewById(R.id.text_message_body_sent);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time_sent);

            receivedLayout = itemView.findViewById(R.id.receivedLayout);
            sentLayout = itemView.findViewById(R.id.sentLayout);
        }

        void bindReceived(Message message) {
            messageTextReceived.setText(message.getText());

            // Format the stored timestamp into a readable String using method.
            timeTextReceived.setText(Utils.getFormattedDate(message.getTimeStamp()));

            sentLayout.setVisibility(View.GONE);

        }

        void bindSent(Message message) {
            messageText.setText(message.getText());

            // Format the stored timestamp into a readable String using method.
            timeText.setText(Utils.getFormattedDate(message.getTimeStamp()));
            receivedLayout.setVisibility(View.GONE);
        }
    }


}
