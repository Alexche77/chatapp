package dev.alvaro.ni.chatapp.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import dev.alvaro.ni.chatapp.R;
import dev.alvaro.ni.chatapp.entities.Chat;
import dev.alvaro.ni.chatapp.ui.utilities.onChatClicked;
import dev.alvaro.ni.chatapp.utilities.Utils;

public class ChatListAdapter extends FirestoreRecyclerAdapter<Chat, ChatListAdapter.ChatHolder> {


    private final onChatClicked mListener;

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirestoreRecyclerOptions} for configuration options.
     *
     * @param options test
     * @param listener test
     */
    public ChatListAdapter(@NonNull FirestoreRecyclerOptions<Chat> options, onChatClicked listener) {
        super(options);
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new instance of the ViewHolder, in this case we are using a custom
        // layout called R.layout.message for each item
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_item, parent, false);

        return new ChatHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull ChatHolder holder, int position, @NonNull Chat model) {
        holder.chatTitle.setText(model.getRecipientName());
        holder.chatLastMessage.setText(model.getMessageText());
        holder.chatTime.setText(Utils.getFormattedDate(model.getTimestamp()));
        holder.rootview.setOnClickListener(v -> mListener.onChatOpened(model));
    }

    class ChatHolder extends RecyclerView.ViewHolder {
        private final View rootview;
        private final TextView chatTitle;
        private final TextView chatTime;
        private final TextView chatLastMessage;

        ChatHolder(View view) {
            super(view);
            rootview = view;
            chatTitle = view.findViewById(R.id.chatTitle);
            chatTime = view.findViewById(R.id.chatTime);
            chatLastMessage = view.findViewById(R.id.lastMessage);
        }
    }
}
