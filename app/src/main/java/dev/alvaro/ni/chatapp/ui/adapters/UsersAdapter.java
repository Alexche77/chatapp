package dev.alvaro.ni.chatapp.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import dev.alvaro.ni.chatapp.R;
import dev.alvaro.ni.chatapp.entities.User;
import dev.alvaro.ni.chatapp.ui.utilities.onUserInteractionListener;
import dev.alvaro.ni.chatapp.utilities.Utils;
public class UsersAdapter extends FirestoreRecyclerAdapter<User,UsersAdapter.ViewHolder> {
    private final onUserInteractionListener mListener;
    private final String currentUser;
    private final Context mContext;

    public UsersAdapter(@NonNull FirestoreRecyclerOptions<User> options, onUserInteractionListener listener, String uid, Context mContext) {
        super(options);
        this.mListener = listener;
        this.currentUser = uid;
        this.mContext = mContext;
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder viewHolder, int i, @NonNull User user) {
        if (!user.getUid().equals(currentUser)){
            viewHolder.userName.setText(user.getName());
            viewHolder.itemView.setOnClickListener(v -> mListener.onUserTouched(user));
        }else{
            viewHolder.userName.setText("You");
        }
        viewHolder.lastConnection.setText(mContext.getString(R.string.last_connection,Utils.getFormattedDate(user.getLastConnection())));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_item, parent, false));
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView userName;
        final TextView lastConnection;
        final View rootView;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            rootView = itemView;
            lastConnection = itemView.findViewById(R.id.lastConnected);
            userName = itemView.findViewById(R.id.userName);
        }
    }
}
