package dev.alvaro.ni.chatapp.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Arrays;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dev.alvaro.ni.chatapp.R;
import dev.alvaro.ni.chatapp.entities.Chat;
import dev.alvaro.ni.chatapp.entities.User;
import dev.alvaro.ni.chatapp.ui.adapters.ChatListAdapter;
import dev.alvaro.ni.chatapp.ui.dialogs.NewChatWizard;
import dev.alvaro.ni.chatapp.ui.utilities.onChatClicked;
import dev.alvaro.ni.chatapp.utilities.DatabaseReferences;

public class ChatList extends AppCompatActivity implements onChatClicked {

    private static final String TAG = ChatList.class.getSimpleName();

    private static final int CHAT_SIGN_IN = 7717;
    private static final String ARG_CHAT_UID = "ARG_CHAT_UID";
    private static final String ARG_CHAT_RECIPIENT_UID = "ARG_CHAT_RECIPIENT_UID";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ChatListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = firebaseAuth -> {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user != null) {
                // User is signed in
                Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
            } else {
                Log.d(TAG, "onAuthStateChanged:signed_out");
            }
            updateUiStatus(user);
        };

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        if (adapter != null)
            adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        if (adapter != null)
            adapter.stopListening();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.log_out:
                logOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logOut() {
        mAuth.signOut();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mAuth.getCurrentUser() != null) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.main_menu, menu);
        }
        return true;
    }

    private void updateUiStatus(FirebaseUser user) {
        invalidateOptionsMenu();
        if (user != null) {
            setContentView(R.layout.activity_chat_list);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            FloatingActionButton fab = findViewById(R.id.fab);
            fab.setOnClickListener(view -> showNewChatWizard());
            User myUser = new User();
            myUser.setLastConnection(System.currentTimeMillis());
            myUser.setName(user.getDisplayName());
            myUser.setUid(user.getUid());

            Uri photoUrl = user.getPhotoUrl();
            if (photoUrl != null) {
                myUser.setProfilePic(photoUrl.getPath());
            } else
                myUser.setProfilePic(null);

            ActionBar ab = getSupportActionBar();
            if (ab != null) {
                ab.setTitle(getString(R.string.chats,myUser.getName()));
            }else{
                Log.e(TAG, "updateUiStatus: Action bar is null");
            }


            CollectionReference mUsersRef = FirebaseFirestore.getInstance()
                    .collection(DatabaseReferences.USERS);
            mUsersRef.document(myUser.getUid()).get().addOnCompleteListener(
                    task -> {
                        if (task.isSuccessful()){
                            DocumentSnapshot ds = task.getResult();
                            if (ds == null || !ds.exists()){
                                Log.i(TAG, "onComplete: Es un usuario nuevo");
                                myUser.setRegisteredAt(System.currentTimeMillis());
                            }
                            mUsersRef.document(myUser.getUid()).set(myUser);
                        }
                    });


            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        InstanceIdResult t = task.getResult();
                        if (t != null) {
                            String s = t.getToken();
                            String uid = FirebaseAuth.getInstance()
                                    .getUid();
                            if (uid != null) {

                                mUsersRef.document(uid)
                                        .update("token", s)
                                        .addOnCompleteListener(task1 -> {
                                            String msg = getString(R.string.token_sent);
                                            Log.d(TAG, "Token == "+msg);
                                        });

                            }

                        }


                    });
            Query mChatQuery = FirebaseFirestore.getInstance()
                    .collection(DatabaseReferences.USERS)
                    .document(user.getUid())
                    .collection(DatabaseReferences.LATEST_CHATS)
                    .orderBy(Chat.MESSAGE_TIMESTAMP);
            FirestoreRecyclerOptions<Chat> options =
                    new FirestoreRecyclerOptions.Builder<Chat>()
                            .setQuery(  mChatQuery, Chat.class)
                            .build();
            adapter = new ChatListAdapter(options, this);

            RecyclerView chatRecycler = findViewById(R.id.chatList);
            chatRecycler.setLayoutManager(new LinearLayoutManager(this));
            chatRecycler.setAdapter(adapter);

            adapter.startListening();

        } else {
            setContentView(R.layout.not_logged_in);
            findViewById(R.id.btnLogin).setOnClickListener(v -> {
                if (mAuthListener != null) {
                    mAuth.removeAuthStateListener(mAuthListener);
                }
                startActivityForResult(
                        AuthUI.getInstance().createSignInIntentBuilder()
                                .setIsSmartLockEnabled(false)
                                .setAvailableProviders(
                                        Arrays.asList(new AuthUI.IdpConfig.GoogleBuilder().build(),
                                                new AuthUI.IdpConfig.EmailBuilder().build())
                                )
                                .setLogo(R.mipmap.ic_launcher)
                                .build(),
                        CHAT_SIGN_IN);
            });
        }

    }

    private void showNewChatWizard() {
        FragmentManager fm = getSupportFragmentManager();
        NewChatWizard newChatWizard = NewChatWizard.newInstance();
        newChatWizard.show(fm, NewChatWizard.class.getSimpleName());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHAT_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                mAuth.addAuthStateListener(mAuthListener);
            } else {
                // Sign in failed
                if (response == null) {
                    showSnackbar(R.string.sign_in_cancelled);
                    return;
                }

                if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
                    showSnackbar(R.string.no_internet_connection);
                    return;
                }

                showSnackbar(R.string.unknown_error);
                Log.e(TAG, "Sign-in error: ", response.getError());
            }
        }
    }

    private void showSnackbar(@StringRes int errorMessageRes) {
        Snackbar.make(findViewById(R.id.root), errorMessageRes, Snackbar.LENGTH_LONG).show();
    }


    @Override
    public void onChatOpened(Chat chat) {
        Log.i(TAG, "onChatOpened: Abriendo la activity para este chat "+chat.getUid());
        Intent i = new Intent(this,ChatActivity.class);
        Bundle b = new Bundle();
        b.putString(ARG_CHAT_UID, chat.getUid());
        b.putString(ARG_CHAT_RECIPIENT_UID, chat.getRecipientUid());
        i.putExtras(b);
        startActivity(i);
    }
}
