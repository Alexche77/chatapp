package dev.alvaro.ni.chatapp.utilities;

public class Constants {
    public static final String CHAT_UID = "CHAT_UID";
    public static final String CHAT_MESSAGE = "CHAT_MESSAGE";
    public static final String CHAT_TITLE = "CHAT_TITLE";
    public static final String NOTIF_TYPE = "NOTIF_TYPE";
    public static final String DEF_NOTIF_ID = "DEF_NOTIF_ID";

    public enum MESSAGE_TYPE {
        DIRECT_MESSAGE("DIRECT_MESSAGE");

        private final String text;


        MESSAGE_TYPE(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }
}
