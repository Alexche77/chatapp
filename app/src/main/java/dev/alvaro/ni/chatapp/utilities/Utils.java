package dev.alvaro.ni.chatapp.utilities;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    @SuppressLint("SimpleDateFormat")
    public static String getFormattedDate(long time){
        return new SimpleDateFormat("yyyy-MM-dd hh:mm a").format(new Date(time));
    }
}
