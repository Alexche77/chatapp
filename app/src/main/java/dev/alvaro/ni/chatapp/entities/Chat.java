package dev.alvaro.ni.chatapp.entities;

import java.util.List;

public class Chat {


    public static final String CHAT_TYPE = "CHAT_TYPE";

    public enum TYPE{
        DM,GROUP;
    }

    public String getRecipientUid() {
        return recipientUid;
    }

    public void setRecipientUid(String recipientUid) {
        this.recipientUid = recipientUid;
    }

    private String recipientUid;

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    private String recipientName;
    private String title;
    private String messageText;
    private long timestamp;

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    private List<String> members;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    private String uid;


    public final static String MEMBERS = "members";
    public final static String TITLE = "title";
    public final static String MESSAGE_TEXT = "messageText";
    public final static String MESSAGE_TIMESTAMP = "timestamp";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Chat() {
    }
}
