package dev.alvaro.ni.chatapp.entities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Members {
    List<String> membersUid;

    public Members() {
    }

    public List<String> getMembersUid() {
        return membersUid;
    }

    public void setMembersUid(List<String> membersUid) {
        this.membersUid = membersUid;
    }

    HashMap<String, Object> result = new HashMap<>();

    public Map<String,Object> toMap() {
        for (String m :
                membersUid) {
            result.put(m,true);
        }

        return result;
    }
}
