package dev.alvaro.ni.chatapp.ui.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dev.alvaro.ni.chatapp.R;
import dev.alvaro.ni.chatapp.entities.Chat;
import dev.alvaro.ni.chatapp.entities.Message;
import dev.alvaro.ni.chatapp.entities.User;
import dev.alvaro.ni.chatapp.ui.adapters.MessageAdapter;
import dev.alvaro.ni.chatapp.utilities.DatabaseReferences;
public class ChatActivity extends AppCompatActivity {
    private static final String ARG_CHAT_UID = "ARG_CHAT_UID";
    private static final String ARG_CHAT_RECIPIENT_UID = "ARG_CHAT_RECIPIENT_UID";
    private String TAG = ChatActivity.class.getName();
    private MessageAdapter mMessageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        if (savedInstanceState == null) {
            Bundle b = getIntent().getExtras();
            if (b != null) {
                String chatUid = b.getString(ARG_CHAT_UID);
                String recipientUid = b.getString(ARG_CHAT_RECIPIENT_UID);

                Log.i(TAG, "onCreate: Cargando el chat con el UID "+chatUid);
                RecyclerView mMessageRecycler = findViewById(R.id.reyclerview_message_list);

                if (chatUid != null && recipientUid != null) {

                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                    if (user != null) {


                        CollectionReference messagesRef = FirebaseFirestore.getInstance()
                                .collection(DatabaseReferences.MESSAGES);

                        FirebaseFirestore.getInstance().collection(DatabaseReferences.USERS).document(recipientUid).get().addOnCompleteListener(
                                task -> {
                                    if (task.isSuccessful()){
                                        DocumentSnapshot ds = task.getResult();
                                        if (ds != null) {
                                            User recipient = ds.toObject(User.class);
                                            if (recipient != null) {
                                                ActionBar ab = getSupportActionBar();
                                                if (ab != null) {
                                                    ab.setTitle(recipient.getName());
                                                }

                                            }
                                        }
                                    }
                                }
                        );
                        Query query = messagesRef.document(chatUid)
                                .collection(DatabaseReferences.CHAT_MESSAGES)
                                .orderBy(Message.TIMESTAMP);
                        FirestoreRecyclerOptions<Message> options =
                                new FirestoreRecyclerOptions.Builder<Message>()
                                        .setQuery(  query, Message.class)
                                        .build();
                        mMessageAdapter = new MessageAdapter(options,user.getUid());

                        mMessageRecycler.setLayoutManager(new LinearLayoutManager(this));
                        mMessageRecycler.setAdapter(mMessageAdapter);

                        Button sendMessage = findViewById(R.id.button_chatbox_send);
                        EditText messageEdit = findViewById(R.id.edittext_chatbox);
                        sendMessage.setOnClickListener(v -> {

                            String message = messageEdit.getText().toString();
                            if (message.isEmpty())
                                messageEdit.setError("Empty messages are not allowed.");
                            else{
                                Log.i(TAG, "onCreate: Mandando mensaje nuevo de "+message);
                                messagesRef.document(chatUid)
                                        .collection(DatabaseReferences.CHAT_MESSAGES).add(Message.build(user,message));

                                Map<String, Object> updatedMap = new HashMap<>();
                                updatedMap.put(Chat.MESSAGE_TEXT,message);
                                updatedMap.put(Chat.MESSAGE_TIMESTAMP,System.currentTimeMillis());

                                FirebaseFirestore.getInstance()
                                        .collection(DatabaseReferences.USERS)
                                        .document(user.getUid())
                                        .collection(DatabaseReferences.LATEST_CHATS)
                                        .document(chatUid)
                                        .update(updatedMap);

                                FirebaseFirestore.getInstance()
                                        .collection(DatabaseReferences.USERS)
                                        .document(recipientUid)
                                        .collection(DatabaseReferences.LATEST_CHATS)
                                        .document(chatUid)
                                        .update(updatedMap);

                                messageEdit.setText("");

                            }
                        });
                    }
                }
            }

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mMessageAdapter != null)
            mMessageAdapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mMessageAdapter != null)
            mMessageAdapter.stopListening();
    }
}
