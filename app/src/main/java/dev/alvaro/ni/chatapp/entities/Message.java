package dev.alvaro.ni.chatapp.entities;

import com.google.firebase.auth.FirebaseUser;

import java.util.HashMap;
import java.util.Map;

public class Message {
    public static final String TIMESTAMP = "timeStamp";

    public static Message build(FirebaseUser user, String message) {
        Message m = new Message();
        m.setSenderName(user.getDisplayName());
        m.setSenderUid(user.getUid());
        m.setText(message);
        m.setTimeStamp(System.currentTimeMillis());
        return m;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderUid() {
        return senderUid;
    }

    public void setSenderUid(String senderUid) {
        this.senderUid = senderUid;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    private String text;
    private String senderName;
    private String senderUid;
    private long timeStamp;

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("text", text);
        result.put("senderName", senderName);
        result.put("senderUid", senderUid);
        result.put("timeStamp", timeStamp);
        return result;
    }

}
