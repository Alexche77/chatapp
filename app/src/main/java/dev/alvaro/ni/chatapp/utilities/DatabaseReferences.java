package dev.alvaro.ni.chatapp.utilities;

public final class DatabaseReferences {
    public static final String USERS = "users";
    public static final String CHATS = "chats";
    public static final String LATEST_CHATS = "latest_chats";
    public static final String MESSAGES = "messages";
    public static final String MEMBERS = "members";
    public static final String CHAT_MESSAGES = "messages_collection";
}
