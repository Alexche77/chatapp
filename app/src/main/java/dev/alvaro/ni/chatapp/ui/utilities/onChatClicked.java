package dev.alvaro.ni.chatapp.ui.utilities;

import dev.alvaro.ni.chatapp.entities.Chat;

public interface onChatClicked {
    void onChatOpened(Chat chat);
}
